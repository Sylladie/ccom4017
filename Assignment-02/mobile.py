#! /usr/bin/env python2
from socket import *
import random
import time

def is_int(i):
	try:
		int(i)
		return True
	except ValueError:
		return False


class Mobile(object):
	mobile_id = None

	def __init__(self, mobile_id):
		self.mobile_id = mobile_id

	def generate_msg(cls):
		random.seed()
		process_time = random.randint(5,15)

		return "%d:%d" % (cls.mobile_id, process_time)


class Client(object):
	server_name = ""
	server_port = None
	client_socket = None

	mobiles = []

	def __init__(self, num_of_mobiles, server, port):
		self.server_name = server
		self.server_port = port
		for i in range(1, num_of_mobiles + 1):
			self.mobiles.append(Mobile(i))

	def run(cls):
		response = ""
		while response != "DONE":
			mobile = cls.mobiles[random.randint(0, len(cls.mobiles))]
			cls.createsocket()
			cls.client_socket.connect((cls.server_name, cls.server_port))
			msg = mobile.generate_msg()
			cls.client_socket.send(msg)

			response = cls.client_socket.recv(1024)
			print response

			cls.client_socket.close()

			time.sleep(random.randint(3,6))

	def createsocket(cls):
		cls.client_socket = socket(AF_INET, SOCK_STREAM)


if __name__ == "__main__":
	import sys

	if len(sys.argv) == 4:
		if is_int(sys.argv[1]) and is_int(sys.argv[3]):
			client = Client(int(sys.argv[1]), sys.argv[2], int(sys.argv[3]))
			client.run()
		else:
			raise ValueError("number of mobiles or port number is not a valid integer")
	else:
		print "USAGE: mobile.py [number of mobiles] [server name] [port number]"