#! /usr/bin/env python2
from socket import *
import threading
import time

class Scheduler(object):
	"""docstring for Scheduler"""

	class Producer(threading.Thread):
		"""
		The Producer class listens to messages generated
		by the mobiles devices and puts the jobs into the
		scheduler queue.
		"""

		socket = None
		port = None
		scheduler_queue = None
		lock = None
		full = None
		empty = None
		amount_jobs = 0

		def __init__(self, port, queue, amountJobs, lock, semaphoreFull, semaphoreEmpty):
			"""
			Producer Constructor

			@param port The port to which the Producer will be listening
			@param queue resource
			@param amountJobs The amount of jobs the Producer will accept before signaling DONE and exiting
			@param lock mutex
			@param semaphoreFull semaphore
			@param semaphoreEmpty semaphore
			"""
			super(Scheduler.Producer, self).__init__()

			self.port = port
			self.socket = socket(AF_INET, SOCK_STREAM)
			self.socket.bind(('', self.port))

			self.scheduler_queue = queue
			self.lock = lock
			self.amount_jobs = amountJobs

			self.full = semaphoreFull
			self.empty = semaphoreEmpty

			# self.daemon = True

		def run(cls):
			cls.socket.listen(1)
			print "Listening to port: %d" % (cls.port)

			jobs_recv = 0

			while jobs_recv < cls.amount_jobs:
				connection_socket, addr = cls.socket.accept()
				message = connection_socket.recv(2048)

				print "Received %s from %s" % (message, addr[0])

				job = message.split(":")
				job = (int(job[0]), int(job[1]))
				
				cls.empty.acquire()
				with cls.lock:
					cls.scheduler_queue.append(job)
				cls.full.release()

				connection_socket.send("OK")
				jobs_recv += 1

				print cls.scheduler_queue

			connection_socket.send("DONE")
			connection_socket.close()


	class Consumer(threading.Thread):
		"""The Consumer class extracts jobs from the scheduler
		queue and executes them."""

		queue = None
		lock = None
		time_spent = {}
		N = None
		full = None
		empty = None
		jobs_done = 0

		def __init__(self, queue, lock, N, semaphoreFull, semaphoreEmpty):
			"""
			Consumer Constructor

			@param queue resource
			@param lock mutex
			@param N 
			@param semaphoreFull semaphore
			@param semaphoreEmpty semaphore
			"""
			super(Scheduler.Consumer, self).__init__()
			self.queue = queue
			self.lock = lock
			self.N = N
			self.full = semaphoreFull
			self.empty = semaphoreEmpty

		def run(cls):
			while cls.jobs_done < cls.N:
				job = None
				cls.full.acquire()
				with cls.lock:
					job = cls.queue.pop()
				cls.empty.release()				
				# do job
				print "Consumer doing job %d:%d" % (job[0], job[1])
				if job[0] not in cls.time_spent:
					cls.time_spent[job[0]] = job[1]
				else:
					cls.time_spent[job[0]] += job[1]
				cls.jobs_done += 1
				print "Consumer: going to sleep for %d seconds" % (job[1])
				time.sleep(job[1])
				print "Consumer: waking up"
			for key in cls.time_spent:
				print "Mobile %d spent %d seconds in the CPU" % (key, cls.time_spent[key])

	# Begin Scheduler 			
	server_port = 12397
	producer_thread = None
	consumer_thread = None
	N = 5  # Change this variable to allow the Scheduler to do more jobs
	queue = []

	def __init__(self):
		"""
		Scheduler Constructor
		"""
		lock = threading.Lock()
		semaphoreFull = threading.Semaphore(0)
		semaphoreEmpty = threading.Semaphore(self.N)
		self.producer_thread = self.Producer(self.server_port, self.queue, self.N, lock, semaphoreFull, semaphoreEmpty)
		self.consumer_thread = self.Consumer(self.queue, lock, self.N, semaphoreFull, semaphoreEmpty)

	def run(cls):
		cls.producer_thread.start()
		cls.consumer_thread.start()

		cls.producer_thread.join()
		cls.consumer_thread.join()


if __name__ == "__main__":

	server = Scheduler()

	server.run()